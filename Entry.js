( function( createClass ) { "use strict";

    Object.defineProperty( Pipeline, "Entry", {
        value: createClass( Editor, Pipeline, THREE )
    });

}( function ( editor, lib, three ) {

    const _config = lib.config.entry;
    const _signals = editor.signals;

    _signals.objectChanged.add( function ( object ) {

        if ( object && object instanceof lib.Entry ) object.updatePosition();

    } );

    return class Entry extends three.Mesh {

        constructor( position, pipe ) {

            let widthSegments = 8,
                heightSegments = 4,
                geometry = new three.SphereBufferGeometry( _config.RADIUS, widthSegments, heightSegments ),
                material = new three.MeshBasicMaterial({ color: _config.COLOR });

            position = position || Editor.NULL_POSITION;

            super( geometry, material );

            this.name = _config.NAME;
            this.userData.type = _config.TYPE;
            this.userData.parentPipe = pipe || null;
            this.position.set( position.x, position.y, position.z );
            this.hiddenInGui = false;
            this._isBindedOn = null;
            this._isReadyToBind = false;
            this._isBinded = false;

        }

        //get hiddenInGui() { return _config.HIDDEN_IN_GUI; }
        get parentPipe() { return this.userData.parentPipe; }
        get notRemovable() { return true; }
        get isEntry() { return this.constructor === Entry /*Entry.prototype.constructor*/; }

        get isReadyToBind() { return this._isReadyToBind; }
        set isReadyToBind( value ) {

            if ( value === true ) {

                this.material.color.set( _config.COLOR_BINDING );
                this.scale.copy( _config.SCALE_BINDING );

            } else {

                this.deselect();

            }

            this._isReadyToBind = false;

        }

        get isBinded() { return this._isBinded; }
        set isBinded( value ) {

            if ( value === true ) {

                this.hide();
                this._isBinded = value;

            } else {

                this.show();
                this.isReadyToBind = false;
                this._isBindedOn = null;

            }

        }

        get isBindedOn() { return this._isBindedOn; }
        set isBindedOn( object ) {

            if ( object !== null && object.isConnection ) {

                this._isBindedOn = object;
                this.isBinded = true;

            } else {

                this.isBinded = false;

            }

        }

        updatePosition() {

            this.parentPipe.updatePosition();

        }

        //TODO: add check on .subtype
        select() {

            if ( this.isReadyToBind ) return;

            let type = this.userData.type.toLowerCase();
            this.scale.copy( lib.config[ type ].SCALE_ACTIVE );
            this.material.color.set( lib.config[ type ].COLOR_ACTIVE );
            return this;

        }

        //TODO: add check on .subtype
        deselect() {

            if ( this.isReadyToBind ) return;

            let type = this.userData.type.toLowerCase();
            this.scale.copy( lib.config[ type ].SCALE_NORMAL );
            this.material.color.set( lib.config[ type ].COLOR );
            return this;

        }

        show() {

            this.visible = true;
            this.hiddenInGui = false;
            return this;

        }

        hide() {

            this.visible = false;
            this.hiddenInGui = true;
            return this;

        }
/*
        toJSON() {

            const self = this;
            let data = THREE.Mesh.prototype.toJSON.call( self );
            let userData;
            if ( data.object.userData === undefined ) data.object.userData = [];

            userData = data.object.userData;
            userData.position = self.position;
            userData.parentPipeUuid = self.parentPipe.uuid;

            return data;

        }

        fromJSON( data ) {

            const _userData = data.userData;
            let parentPipe = Editor.selectByUuid( _userData.parentPipeUuid );
            let entry = new Pipeline.Entry( _userData.position, parentPipe );

            entry.userData = _userData;
            Editor.execute( new AddObjectCommand( entry ));
            entry.uuid = data.uuid;

        }*/

    }

}));