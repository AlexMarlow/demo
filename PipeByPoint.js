( function( module ) { "use strict";

    Object.defineProperty( Interface, "PipeByPoint", {
        value: module( Editor, Pipeline, THREE, signals )
    });

}( function ( Editor, Pipeline, THREE, signals ) {

    const _signals = Editor.signals;
    const selectedObject = () => Editor.selected;
    // основа для кнопок
    const _button = ( label, onClick ) =>
        new UI.Button( label ).assignStyles( {
            width: '100%'
        } ).onClick( () => onClick() );

    const getConnectionLevel = object => {

        switch ( object.constructor ) {

            case Pipeline.Connection:
                return 2;

            case Pipeline.Tee:
                return 3;

            case Pipeline.Cross:
                return 4;

        }

    };
    //TODO: возможно разбить на два компонента
    class PointRow extends UI.Row {

        /**
         * @constructor
         * @param {String} label
         * @param {Function} onChange
         */
        constructor( label, onChange ) {

            super();
            const self = this;

            this.rectCoords = new THREE.Vector3();
            this.cylCoords = new THREE.Cylindrical();
            this.setDisplay( 'flex' );
            this.assignStyles( {
                display: 'flex',
                marginBottom: '4px'
            });

            let text = new UI.Text( label ).setWidth( '30%' );
            let coordsRow = new UI.Row().setWidth( '60%' );
            this.rectCoordsRow = new UI.NumbersRow3( {
                numbers: self.rectCoords,
                onChange: value => {

                    self.setCoords( value );
                    onChange( self.getCoords() );

                }
            } );
            this.cylCoordsRow = new UI.NumbersRow3( {
                numbers: [ self.cylCoords.radius, self.cylCoords.theta, self.cylCoords.y ],
                onChange: value => {

                    this._setCyl( value );
                    this._setRect( self.rectCoords.setFromCylindrical( self.cylCoords ) );
                    onChange( self.getCoords() );

                }
            } );
            this.cylCoordsRow.numbers[1].setRange( -360, 360 ); //ограничение значений для угла поворота
            let resetIcon = new UI.Text( "\u21BA" )
                .assignStyles({
                    width: '10%',
                    cursor: 'pointer'
                })
                .onClick( () => {
                    self.setCoords( Editor.NULL_POSITION );
                    onChange( self.rectCoords );
                });

            this.add(
                text,
                coordsRow.add(
                    self.rectCoordsRow,
                    self.cylCoordsRow
                ),
                resetIcon
            );

        }

        _setRect( value ) {

            this.rectCoords.copy( value );
            this.rectCoordsRow.setValue( this.rectCoords );

        }

        _setCyl( value ) {

            if ( value ) {

                if ( value instanceof THREE.Vector3 ) this.cylCoords.set( value.x, THREE.Math.degToRad( value.y ), value.z );
                else if ( value instanceof THREE.Cylindrical ) this.cylCoords.copy( value );

            } else {

                this.cylCoords.setFromVector3( this.rectCoords )

            }

            this.cylCoordsRow.setValue( [ this.cylCoords.radius, THREE.Math.radToDeg( this.cylCoords.theta ), this.cylCoords.y ] );

        }

        getCoords() {

            return this.rectCoords;

        }

        setCoords( value ) {

            this._setRect( value );
            this._setCyl();

        }

    }

    let isActive = false;
    let lastAdded = null;

    let container = new UI.Panel();
    let title = new UI.Title( 'Создание трубы по точкам' );
    let btn_addByCoords = _button( 'По координатам', handler_addByCoords );
    let btn_add = _button( 'Добавить участок', handler_add ).setDisabled( true );

    let params = {
        title: title,
        name: 'PipeByPoint'
    };

    container.$hide(); //TODO: отключение в Sidebar

    // length

    let pipeLenRow = new UI.Row().add(
        new UI.Text( 'Длина' ).setWidth( '30%' )
    );
    let pipeLen = new UI.Number( 0 ).setDisabled( true );

    pipeLenRow.add( pipeLen );

    // 1st point

    let startRow = new PointRow( 'Начало', value => updatePointPosition( 'start', value ) );

    // 2nd point

    let endRow = new PointRow( 'Конец', value => updatePointPosition( 'end', value ) );

    container.add(
        title,
        pipeLenRow,
        startRow,
        endRow,
        btn_addByCoords,
        btn_add
    );

    // Signals

    _signals.runPipeByPoint.add( value => {

        if ( value === true ) {

            isActive = true;
            Editor.showDialog( new Dialog( { container, params } ) );

        } else {

            isActive = false;
            container.$hide();

        }

    });

    _signals.objectSelected.add( object => updateUI( object ) );
    //_signals.objectChanged.add( function ( object ) { updateUI( object ); } );
    _signals.refreshSidebarObject3D.add( object => updateUI( object ) );

    // Functions

    //TODO: optimize
    function handler_add() {

        let _selectedObject = selectedObject();
        let startPosition = _selectedObject.position;
        let newPipe = new Pipeline.Pipe( startPosition, startPosition );
        let entries = [];
        let connection;
        let connectionLevel = getConnectionLevel( _selectedObject );

        if ( _selectedObject.isConnection && _selectedObject.bounds.length < connectionLevel ) {

            entries = _selectedObject.bounds;
            entries.push( newPipe.entries.start );
            _selectedObject.bindEntries( entries );
            Editor.execute( new AddObjectCommand( newPipe ) );
            Editor.select( newPipe.entries.end );
            lastAdded = newPipe.entries.end;

        } else {

            switch ( _selectedObject.constructor ) {

                case Pipeline.Entry:
                    connection = new Pipeline.Connection( startPosition );
                    entries.push( _selectedObject );
                    break;

                case Pipeline.Connection:
                    connection = new Pipeline.Tee( startPosition );
                    entries = _selectedObject.bounds;
                    _selectedObject.unbind( true );
                    break;

                case Pipeline.Tee:
                    connection = new Pipeline.Cross( startPosition );
                    entries = _selectedObject.bounds;
                    _selectedObject.unbind( true );
                    break;

            }

            entries.push( newPipe.entries.start );
            connection.bindEntries( entries );
            Editor.execute( new AddObjectCommand( connection ) );
            Editor.execute( new AddObjectCommand( newPipe ) );
            Editor.select( newPipe.entries.end );
            lastAdded = newPipe.entries.end;

        }

    }

    function handler_addByCoords() {

        let start, end, object;

        start = startRow.getCoords();
        end = endRow.getCoords();
        object = new Pipeline.Pipe( start, end );

        Editor.execute( new AddObjectCommand( object ) );
        Editor.select( object.entries.end );
        lastAdded = object.entries.end;

    }

    function updatePointPosition( point, value ) {

        if ( selectedObject() == null || ! selectedObject().isEntry ) return;

        let pipe = selectedObject().parentPipe;
        let pipeEnd = pipe.entries[ point ];

        pipeEnd.position.copy( value );
        _signals.objectChanged.dispatch( pipeEnd );
        pipeEnd.updatePosition();
        pipeLen.setValue( pipe.fullLength );

    }
    function updateUI( object ) {

        if ( isActive !== true ) return;
        if ( object !== null && object instanceof Pipeline.Entry ) {

            if ( object.isConnection ) {

                let connectionLevel = getConnectionLevel( object );

                if ( object.bounds.length === connectionLevel ) btn_add.setDisabled( object.isCross );
                else if ( object.bounds.length < connectionLevel ) btn_add.setDisabled( false );

            } else btn_add.setDisabled( false );

            if ( object.isEntry ) {

                let parentPipe = object.parentPipe;
                let start = parentPipe.startPosition;
                let end = parentPipe.endPosition;
                let len = parentPipe.fullLength;

                startRow.setCoords( start );
                endRow.setCoords( end );
                pipeLen.setValue( len );

            }

        } else {

            btn_add.setDisabled( true );

            startRow.setCoords( Editor.NULL_POSITION );
            endRow.setCoords( Editor.NULL_POSITION );
            pipeLen.setValue( 0 );

            lastAdded = null;

        }

    }

    return { container, title };

}));